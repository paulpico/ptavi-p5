Práctica 5 - Sesión SIP
Protocolos para la Transmisión de Audio y Vı́deo en Internet
Versión 8.0.1 - 6.11.2017

Ejercicios

Creación de repositorio para la práctica

1. Con el navegador, dirı́gete al repositorio ptavi-p5 en la cuenta del
profesor en GitHub1 y realiza un fork, de manera que consigas tener
una copia del repositorio en tu cuenta de GitHub. Clona el repositorio
que acabas de crear a local para poder editar los archivos. Trabaja a
partir de ahora en ese repositorio, sincronizando los cambios que vayas
realizando.

Como tarde al final de la práctica, deberás realizar un push para subir
tus cambios a tu repositorio en GitHub. En esta práctica, al contrario
que con las demás, se recomienda hacer frecuentes commits, pero el
push al final.

Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que
se puede abrir con Wireshark2 . Se pide rellenar las cuestiones que se
plantean en este guión en el fichero p5.txt que encontrarás también
en el repositorio.

2. Observa que las tramas capturadas corresponden a una sesión SIP
con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes
cuestiones:
* ¿Cuántos paquetes componen la captura?
    954 paquetes componen la captura.
* ¿Cuánto tiempo dura la captura?
    56.149 segundos dura la captura.
* ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se
trata de una IP pública o de una IP privada? ¿Por qué lo sabes?
    192.168.1.34 es la IP de la máquina donde se ha efectuado la captura. Se trata de una IP privada, ya que 192.168.x.x corrresponde al rango de direcciones privadas.
3. Antes de analizar las tramas, mira las estadı́sticas generales que aparecen en el menú de Statistics. En el apartado de jerarquı́a de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico
correspondiente al protocolo TCP y UDP.
* ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando
de una aplicación que transmite en tiempo real?
    Es mayor el porcentaje de UDP(96.2%) al de TCP(2.1%). Si tiene sentido, ya que las transmisiones a tiempo real utilizan UDP, dando importancia a la rapidez que llegan los paquetes y no a que lleguen todos los bytes del paquete.
* ¿Qué otros protocolos podemos ver en la jerarquı́a de protocolos?
¿Cuales crees que son señal y cuales ruido?
    Ethernet: IPv4
    UDP: STUN, SIP, RTP-> H.261, RTCP, DNS
    TCP: HTTP, ICMP
    ARP
    
    Señal seria RTP y el resto ruido
    
4. Observa por encima el flujo de tramas en el menú de Statistics en IO
Graphs. La captura que estamos viendo incluye desde la inicialización
(registro) de la aplicación hasta su finalización, con una llamada entremedias.
* Filtra por sip para conocer cuándo se envı́an paquetes SIP. ¿En
qué segundos tienen lugar esos envı́os?
    Tiene lugar los envios en los segundos 7, 14, 16, 38, 39 y 54. 
* Y los paquetes con RTP, ¿cuándo se envı́an?
    Desde el segundo 7 al 38.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

5. Analiza las dos primeras tramas de la captura.
* ¿Qué servicio es el utilizado en estas tramas?
    El servicio utilizado es DNS.
* ¿Cuál es la dirección IP del servidor de nombres del ordenador
que ha lanzado Ekiga?
    La dirección IP es 80.58.61.250
* ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres?
    Devuelve la dirección IP 86.64.162.35
6. A continuación, hay más de una docena de tramas TCP/HTTP.
* ¿Podrı́as decir la URL que se está pidiendo?
    La URL que se esta pidiendo la podemos ver en el paquete 8 (HTTP) con el método Get que es http://ekiga.net/ip/.
* ¿Qué user agent (UA) la está pidiendo?
    La esta pidiendo el User-Agent: Ekiga\r\n
* ¿Qué devuelve el servidor?
     Devuelve un 200 OK en el paquete 10 y el contenido es text/html que tiene la dirección IP 83.36.48.212
* Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y
vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo
que está respondiendo el servidor?
    Recibimos la dirección IP 212.128.255.47, que supongo que es la IP de mi ordenador. Y esta comprobando si es mi ip pública o privada. 
7. Hasta la trama 45 se puede observar una secuencia de tramas del
protocolo STUN.
* ¿Por qué se hace uso de este protocolo?
    Este protocolo se usa para poder realizar un intercambio de paquetes UDP a través de dispositivos NAT. Nos permite saber nuestra IP pública, tipo de NAT y puerto.
* ¿Podrı́as decir si estamos tras un NAT o no?
    Si, porque estamos utilizando el protocolo CLASSIC-STUN.
8. La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al
realizar una llamada. Por eso, todo usuario registra su localización en
un servidor Registrar. El Registrar guarda información sobre los
usuarios en un servidor de localización que puede ser utilizado para
localizar usuarios.
* ¿Qué dirección IP tiene el servidor Registrar?
    Tiene la dirección IP 86.64.162.35
* ¿A qué puerto (del servidor Registrar) se envı́an los paquetes
SIP?
    Al puerto 5060.
* ¿Qué método SIP utiliza el UA para registrarse?
     Utiliza el método REGISTER.
* Además de REGISTER, ¿podrı́as decir qué instrucciones SIP entiende el UA?
    El UA entiende ademas de REGISTER, las instrucciones SIP: INVITE, ACK, OPTIONS, BYE, CANCEL, NOTIFY, REFER y MESSAGE.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

9. Fijémonos en las tramas siguientes a la número 46:
* ¿Se registra con éxito en el primer intento?
    No, ya que en el paquete 50 le envia un 401 Unauthorized.
* ¿Cómo sabemos si el registro se ha realizado correctamente o no?
    Lo sabemos en el segundo intento, en el paquete 54 que envia un 200 OK.
* ¿Podrı́as identificar las diferencias entre el primer intento y el
segundo de registro? (fı́jate en el tamaño de los paquetes y mira
a qué se debe el cambio)
    La diferencia es que en el segundo intento (tamaño 712 bytes) incluye en la cabecera el campo "Authorization" y en el primero no (523 bytes).
* ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica
las unidades.
    Miramos en la cabecera, el campo Expires:3600 segundos.
10. Una vez registrados, podemos efectuar una llamada. Vamos a probar
con el servicio de eco de Ekiga que nos permite comprobar si nos
hemos conectado correctamente. El servicio de eco tiene la dirección
sip:500@ekiga.net. Veamos el INVITE de cerca.
* ¿Puede verse el nombre del que efectúa la llamada, ası́ como su
dirección SIP?
    Si se puede, en la cabecera en el campo From: "Gregorio Robles" <sip:grex@ekiga.net>
* ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está?
    Contiene la descripción de la sesión, esta con el formato/protocolo SDP.
* ¿Tiene éxito el primer intento? ¿Cómo lo sabes?
    No, ya que en el paquete 85 envia un 407 Proxy Authentication Required.
* ¿En qué se diferencia el segundo INVITE más abajo del primero? ¿A qué crees que se debe esto?
    Se diferencian en que el segundo INVITE contiene en su cabecera el campo "Proxy-Authorization" y en el primero no.
11. Una vez conectado, estudia el intercambio de tramas.
* ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos?
    RTP y RTP con H.261. RTP es un protocolo para intercambio de datos a tiempo real y H.261 es un estándar de compresión de video que lleva RTP por debajo.
* ¿Cuál es el tamaño de paquete de los mismos?
    Los paquetes de RTP tienen un tamaño de 214 bytes, pero los paquetes H.261 se envian de dos en dos con un tamaño variable de entre 200-1080 bytes aproximadamente.
* ¿Se utilizan bits de padding?
    No, ya que en el campo "Padding" pone que es False.
* ¿Cuál es la periodicidad de los paquetes (en origen; nota que la
captura es en destino)?
    Los paquetes RTP aproximadamente en unos 0.021 seg. y los paquetes H.261 aproximadamente en unos 0.24 seg.
* ¿Cuántos bits/segundo se envı́an?
    Omitiendo los paquetes H.621, se envian 71333.33 bit/seg. Lo que equivale a 71.33 Kbit/seg.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

12. Vamos a ver más a fondo el intercambio RTP. En Telephony hay una
opción RTP. Empecemos mirando los flujos RTP.
* ¿Cuántos flujos hay? ¿por qué?
    Tenemos 2 flujos. Uno es para audio g711A en el puerto 5010 y otro para video h261 en el puerto 5014.
* ¿Cuántos paquetes se pierden?
    No se pierde ningún paquete, el campo "Lost" es igual a 0.
* ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el
valor de delta?
    El valor máximo del delta es 1290.44 ms en RTP y 1290.48 ms en H.261. Y significa que es la latencia el tiempo que tarda en enviarse un paquete.
* ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué
quiere decir eso? ¿Crees que estamos ante una conversación de
calidad?
    En RTP tiene 119.64 ms de jitter máximo y 42.50 ms de jitter medio y en H.261 tiene 183.10 ms de jitter máximo y 153.24 ms de jitter medio. El jitter refleja la latencia máxima y la latencia de media de la comunicación. Con estos valores si estariamos ante una conversacion de calidad.
13. Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony
-> RTP -> Stream Analysis.
* ¿Cuánto valen el delta y el jitter para el primer paquete que
ha llegado?
    En ambos casos vale 0.00 ms.
* ¿Podemos saber si éste es el primer paquete que nos han enviado?
    Si, ya que el numero de secuencia es el menor de todos y la delta es 0.
* Los valores de jitter son menores de 10ms hasta un paquete
dado. ¿Cuál?
    Hasta el paquete 246.
* ¿A qué se debe el cambio tan brusco del jitter?
    A que se produjo una descarga.
* ¿Es comparable el cambio en el valor de jitter con el del delta?
¿Cual es más grande?
    El jitter depende de la delta por lo tanto es comparable que los dos aumenten. Es mas grande el cambio del delta.
14. En Telephony selecciona el menú VoIP calls. Verás que se lista la
llamada de voz IP capturada en una ventana emergente. Selecciona
esa llamada y pulsa el botón Graph.
* ¿Cuánto dura la conversación?
    Dura 24 segundos.
* ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs?
    Hay un SSRC para cada flujo. Para audio g711A en el puerto 5010, su SSRC es 0xbf4afd37 y para video h261 en el puerto 5014, su SSRC es 0x43306582. No hay CSRCs.
15. Identifica la trama donde se finaliza la conversación.
* ¿Qué método SIP se utiliza?
    El método BYE.
* ¿En qué trama(s)?
    En las tramas 924, 925, 927 y 933.
* ¿Por qué crees que se envı́a varias veces?
    Nos llegan paquetes ICMP, port unreachable. El usuario intenta contactar con el servidor para finalizar la conexión. Envia 4 BYEs hasta que alcanza su destino y le devuelve un SIP 200 OK (Trama 938 a 941).
16. Finalmente, se cierra la aplicación de VozIP.
* ¿Por qué aparece una instrucción SIP del tipo REGISTER?
    Porque el usuario se da de baja y pone expires igual a 0.
* ¿En qué trama sucede esto?
    En las tramas 950 y 952.
* ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)?
    Pues que aqui le da valor 0 al campo "Expires".
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

Captura de una sesión SIP

17. Dirı́gete a la web http://www.ekiga.net con el navegador y créate
una cuenta. Lanza Ekiga, y configúralo con los datos de la cuenta
que te acabas de crear. Comprueba que estás conectado (En la barra
al final de la ventana podrás ver “Connected”). Al terminar, cierra
completamente Ekiga.

18. Captura una sesión SIP de una conversación con el número SIP sip:500@ekigan.net.
Recuerda que has de comenzar a capturar tramas antes de arrancar
Ekiga para ver todo el proceso3 .

19. Observa las diferencias en el inicio de la conversación entre el entorno
del laboratorio y el del ejercicio anterior4 :
* ¿Se utilizan DNS y STUN? ¿Por qué?
    Se utiliza DNS sólo para saber la IP y el nombre de la máquina, no se utiliza el STUN debido a que ya nos encontramos en una ip pública.
* ¿Son diferentes el registro y la descripción de la sesión?
    No cambia, sólo el nombre sip.
20. Identifica las diferencias existentes entre esta conversación y la conversación anterior:
* ¿Cuántos flujos tenemos?
    Tenemos 3 flujos:
    - SSRC=0x59636C80
    - SSRC=0x27E462B7
    - SSRC=0x67852D11
* ¿Cuál es su periodicidad?
    Tiene una periocidad de 20 ms.
* ¿Cuánto es el valor máximo del delta y los valores medios y
máximo del jitter?
    Para el SSRC=0x59636C80 el valor máximo del delta es 32.81 ms, y valor medio del jitter es 0.23 ms y el valor máximo del jitter es 2.11 ms.
    Para el SSRC=0x27E462B7 el valor máximo del delta es 24.65 ms, y valor medio del jitter es 0.22 ms y el valor máximo del jitter es 0.81 ms.
    Para el SSRC=0x67852D11 el valor máximo del delta es 40.16 ms, y valor medio del jitter es 0.23 ms y el valor máximo del jitter es 6.60 ms.
* ¿Podrı́as reproducir la conversación desde Wireshark? ¿Cómo?
    Si, con un códec de audio adecuado.
Comprueba que poniendo un valor demasiado pequeño para el
buffer de jitter, la conversación puede no tener la calidad necesaria.
* ¿Sabrı́as decir qué tipo de servicio ofrece sip:music@iptel.org?
    Es un servicio de música telefónico.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]


21. Filtra por los paquetes SIP de la captura y guarda únicamente los
paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.
[Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]
[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitHub]

